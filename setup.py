#!/usr/bin/env python
from setuptools import setup, find_packages

setup(name='pypcars2api',
      version='0.2',
      description='Project Cars 2 API implementation for Python 3',
      author='Sim Racing Studio',
      author_email='engineering@simracingstudio.com',
      url='https://gitlab.com/simracingstudio/pypcars2api/',
      keywords='games racing projectcars',
      packages=find_packages(),      
     )
